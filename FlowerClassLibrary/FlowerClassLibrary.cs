﻿using System;
using System.Collections.Generic;

namespace FlowerClassLibrary
{
    // Описываем класс букета
    public class Bouquet
    {

        private List<AbstractFlower> _flowers = new List<AbstractFlower>(); // Набор цветов, содержащихся в букете

        public List<AbstractFlower> Flowers
        {
            get { return _flowers; }
            set { _flowers = value; }
        }

        public void AddFlower(AbstractFlower flower)
        {
            _flowers.Add(flower);
        }

    }
    // Описываем класс абстарктного цветка
    public abstract class AbstractFlower
    {
        private string _name; // Название
        private double _price;   // Цена
        private double _height; // Высота

        public abstract string CheckClass();

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public double Price
        {
            get { return _price; }
            set { _price = value; }
        }
        public double Height
        {
            get { return _height; }
            set { _height = value; }
        }
    }

    // Садовые цветы
    public class Garden : AbstractFlower
    {
        private double _weight; // Вес

        public override string CheckClass()
        {
            return ("Garden");
        }

        public double Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }
    }

    // Полевые цветы
    public class Wild : AbstractFlower
    {
        private string _habitat; // Среда обитания
        private double _age;     // Возраст

        public override string CheckClass()
        {
            return ("Wild");
        }

        public string Habitat
        {
            get { return _habitat; }
            set { _habitat = value; }
        }

        public double Age
        {
            get { return _age; }
            set { _age = value; }
        }
    }

    // Тюльпаны
    public class Tulip : Garden
    {
        private string _color; // Цвет

        public override string CheckClass()
        {
            return ("Tulip");
        }

        public string Color
        {
            get { return _color; }
            set { _color = value; }
        }
    }

    // Розы
    public class Rose : Garden
    {
        private int _freshness; // Свежесть

        public override string CheckClass()
        {
            return ("Rose");
        }

        public int Freshness
        {
            get { return _freshness; }
            set { _freshness = value; }
        }
    }

    // Ромашки
    public class Chamomile : Wild
    {
        private int _numberOfPetals; // Число лепестков

        public override string CheckClass()
        {
            return ("Chamomile");
        }

        public int NumberOfPetals
        {
            get { return _numberOfPetals; }
            set { _numberOfPetals = value; }
        }
    }
}
