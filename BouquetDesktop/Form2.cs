﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FlowerClassLibrary;

namespace BouquetDesktop
{
    public partial class AddForm : Form
    {
        public string Clas;
        public string NameAddForm;
        public string PriceAddForm;
        public string HeightAddForm;
        public DialogResult dr;
        public AddForm()
        {
            InitializeComponent();
        }
        public AddForm(AbstractFlower flower) : this()
        {
            nameTextBox.Text = flower.Name;
            priceTextBox.Text = flower.Price.ToString();
            heightTextBox.Text = flower.Height.ToString();
            classComboBox.SelectedItem = flower.CheckClass();
        }

        private void AddForm_Load(object sender, EventArgs e)
        {
        }

        private void classComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if ((nameTextBox.Text == "") || (priceTextBox.Text == "")
                || (heightTextBox.Text == "") || (classComboBox.SelectedItem == null))
            {
                errorLabel.Text = "You should fill every gap";
            }
            else
            {
                double.TryParse(priceTextBox.Text, out double priceValue);
                double.TryParse(heightTextBox.Text, out double heightValue);
                if ((priceValue < 0) || (heightValue < 0))
                {
                    errorLabel.Text = "Price and height should be positive";
                }
                else
                {
                    errorLabel.Text = "";
                    NameAddForm = nameTextBox.Text;
                    PriceAddForm = priceTextBox.Text;
                    HeightAddForm = heightTextBox.Text;
                    Clas = classComboBox.SelectedItem.ToString();
                    dr = DialogResult.OK;
                    Close();
                }
            }
            
            
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            dr = DialogResult.Cancel;
            Close();
        }

    }
}
