﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FlowerClassLibrary;
using FlowerLogicLibrary;

namespace BouquetDesktop
{
    public partial class MainForm : Form
    {
        private Bouquet bouquet;
        private void ForCalc()
        {
            FlowersListBox.Items.Clear();
            foreach (AbstractFlower flower in bouquet.Flowers)
            {
                FlowersListBox.Items.Add(flower.Name + " - " + flower.Price.ToString() + " руб.");
            }
            PriceCalculator calculator = new PriceCalculator();
            priceLabel.Text = calculator.GetTotalPrice(bouquet).ToString();
        }
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            BouquetFactory myBouquet = new BouquetFactory();
            bouquet = myBouquet.CreateBouquet();
            ForCalc();
        }


        private void addButton_Click(object sender, EventArgs e)
        {
            AddForm temp = new AddForm();
            temp.ShowDialog();
            if (temp.dr == DialogResult.OK)
            {
                BouquetFactory factory = new BouquetFactory();
                AbstractFlower newFlower = factory.CreateFlower(temp.Clas,
                temp.NameAddForm, temp.PriceAddForm, temp.HeightAddForm);
                bouquet.AddFlower(newFlower);
                ForCalc();
            }
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            if (FlowersListBox.SelectedIndex < 0)
            {
                errorLabel.Text = "You should choose any item";
            }
            else
            {
                errorLabel.Text = "";
                AddForm temp = new
                AddForm(bouquet.Flowers[FlowersListBox.SelectedIndex]);
                temp.ShowDialog();
                if (temp.dr == DialogResult.OK)
                {
                    BouquetFactory factory = new BouquetFactory();
                    AbstractFlower newFlower = factory.CreateFlower(temp.Clas,
                    temp.NameAddForm, temp.PriceAddForm, temp.HeightAddForm);
                    bouquet.Flowers[FlowersListBox.SelectedIndex] = newFlower;
                    ForCalc();
                }
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (FlowersListBox.SelectedIndex < 0)
            {
                errorLabel.Text = "You should choose any item";
            }
            else
            {
                errorLabel.Text = "";
                bouquet.Flowers.RemoveAt(FlowersListBox.SelectedIndex);
                ForCalc();
            }
        }

        private void FlowersListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
     
    }
}
