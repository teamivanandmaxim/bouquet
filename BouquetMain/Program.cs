﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlowerClassLibrary;
using FlowerLogicLibrary;

namespace BouquetMain
{
    class Program
    {
        static void Main(string[] args)
        {
            BouquetFactory factory = new BouquetFactory();
            Bouquet bouquet = factory.CreateBouquet();
            BouquetPrinter bouquetPrint = new BouquetPrinter();
            bouquetPrint.PrintBouquet(bouquet);
        }
    }
}
