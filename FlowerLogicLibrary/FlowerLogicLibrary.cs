﻿using System;
using FlowerClassLibrary;

namespace FlowerLogicLibrary
{
    // Описываем фабрику букетов
    public class BouquetFactory
    {
        public AbstractFlower CreateFlower(string clas, string name, string price, string height)
        {
            AbstractFlower flower = clas switch
            {
                "Tulip" => new Tulip(),
                "Rose" => new Rose(),
                "Chamomile" => new Chamomile(),
                "Wild" => new Wild(),
                "Garden" => new Garden(),
                _ => new Tulip(),
            };
            flower.Name = name;
            flower.Price = double.Parse(price);
            flower.Height = double.Parse(height);
            return flower;
        }

        public Bouquet CreateBouquet()
        {
            Bouquet bouquet = new Bouquet();

            Tulip tulip = new Tulip();
            tulip.Name = "Тюльпан";
            tulip.Price = 60;
            tulip.Height = 0.3;
            tulip.Weight = 0.05;
            tulip.Color = "Желтый";
            bouquet.AddFlower(tulip);

            Rose rose = new Rose();
            rose.Name = "Роза";
            rose.Price = 100;
            rose.Height = 0.5;
            rose.Weight = 0.07;
            rose.Freshness = 80;
            bouquet.AddFlower(rose);

            Chamomile chamomile = new Chamomile();
            chamomile.Name = "Ромашка";
            chamomile.Price = 10;
            chamomile.Height = 0.2;
            chamomile.Habitat = "Поле";
            chamomile.Age = 0.1;
            chamomile.NumberOfPetals = 16;
            bouquet.AddFlower(chamomile);

            Wild orchid = new Wild();
            orchid.Name = "Орхидея";
            orchid.Price = 500;
            orchid.Height = 0.15;
            orchid.Habitat = "Лес";
            orchid.Age = 0.5;
            bouquet.AddFlower(orchid);

            return bouquet;
        }
    }

    // Описываем логику подсчета стоимости цветов
    public class PriceCalculator
    {
        public double GetTotalPrice(Bouquet bouquet)
        {
            double totalPrice = 0.0;
            foreach (AbstractFlower flower in bouquet.Flowers)
            {
                totalPrice += flower.Price;

            }
            return totalPrice;
        }
    }

    // Описываем логику вывода
    public class BouquetPrinter
    {
        public void PrintFlower(AbstractFlower flower)
        {
            Console.Write("\tНазвание: " + flower.Name + ", стоимость: " + 
                (flower.Price).ToString() +  ", высота: " + (flower.Height).ToString() + ";\n");
        }
        public void PrintBouquet(Bouquet bouquet)
        {
            Console.Write("Цветы в букете: \n");
            foreach (AbstractFlower flower in bouquet.Flowers)
            {
                PrintFlower(flower);
            }
            PriceCalculator calculator = new PriceCalculator();
            Console.Write("Общая стоимость цветов в букете: " + calculator.GetTotalPrice(bouquet).ToString());
        }
    }
}
